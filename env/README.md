# Introduction
This repository contains a Kameleon recipe to build environments.  
This recipe is used to build a Grid'5000 image, which is used to execute
simulated instances on the Grid'5000 testbed.

# Building the image

This image is supposed to be built on a user's machine, assuming the user has
downloaded a Grid'5000 debian image.

Different parameters such as paths and Grid'5000 user name must be adjusted in
[batsim_g5k.yaml](batsim_g5k.yaml).

``` bash
kameleon build batsim_g5k.yaml
```
# Using the image on Grid'5000

## Pushing the image

``` bash
scp ~/my_g5k_images/batsim_g5k* grenoble.g5k:~/my_g5k_images/
```

## Connecting to Grid'5000

``` bash
ssh grenoble.g5k
```

## Configuring the image

The description file (.yaml in ``~/my_g5k_images/``) contains an absolute path
that must be modified to match your Grid'5000 home.

## Registering the image
In order to execute a job which deploys this image, it must first be registered.

``` bash
kaenv3 -a ~/my_g5k_images/batsim_g5k_bd9912b275d7.yaml
```
