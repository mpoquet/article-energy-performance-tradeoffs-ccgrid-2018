This repository contains material to replicate the experimental process done
for the *Performance vs Energy Tradeoffs via Shutdown Policies in EASY
Backfilling* article submitted to the IEEE/ACM CCGrid 2018 conference.

- the [env directory](./env) contains Kameleon scripts to build the software
  stack (operating system + binaries) that has been used to produce our
  simulation data.
- the [expe directory](./expe) contains a programmatic description of the
  experimental process conducted for the article, and a short description
  about how to execute it.
- the [expe/expe_out4 directory](./expe/expe_out4) contains aggregated data
  resulting from our experimental process, as well as the corresponding figures.
- the [article directory](./article) contains the latex files used to
  generate the article.
